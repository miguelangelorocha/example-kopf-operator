## Sample operator

### Creation of a Database CRD

```
$ kubectl apply -f crd.yml
```

### Creation of RBAC rules

```
apiVersion: v1
kind: ServiceAccount
metadata:
  name: db-operator
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: db-operator
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
  - kind: ServiceAccount
    name: db-operator
    namespace: default
```

```
$ kubectl apply -f rbac.yml
```

### Deployment of the operator

```
$ kubectl apply -f operator.yml
```

### Creation of sample database objects

```
$ kubectl apply -f obj/mongo.yml
$ kubectl apply -f obj/mysql.yml
```

We can then make sure the Pods and NodePort Services have been created for each Database object

```
$ kubectl get pod,svc
```

### Deletion of the Database objects

```
$ kubectl delete -f obj/mongo.yml
$ kubectl delete -f obj/mysql.yml
```

Checking once again the list of Pods and Services

```
$ kubectl get pod,svc
```
